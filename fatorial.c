#include "fatorial.h"

int fatorial(int x){
	int resultado;
		if(x < 0 || x == 0){
			return -1;
		}
		else if(x == 1){
			resultado = 1;
			return resultado;
		}
		else if(x >= 2){
			for(resultado=2; x>2; x=x-1)
				resultado = x * resultado;
			}
		return resultado;
		
}
